// const willGetAPlayStation = new Promise((resolve, reject) => {
// 	const randomNum = Math.random();

// 	if (randomNum > 0.5) {
// 		resolve();
// 	} else {
// 		reject();
// 	}
// });

// willGetAPlayStation
// 	.then(() => {
// 		console.log('Thank you uncle!');
// 	})
// 	.catch(() => {
// 		console.log('F you uncle!');
// 	});

// function playStationPromise() {
// 	return new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			const randomNum = Math.random();

// 			if (randomNum > 0.5) {
// 				resolve();
// 			} else {
// 				reject();
// 			}
// 		}, 5000);
// 	});
// }

// playStationPromise()
// 	.then(() => {
// 		console.log('Promise was fulfilled!');
// 	})
// 	.catch(() => {
// 		console.log('Promise was rejected!');
// 	});

function fakeRequest(url) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			const pages = {
				'/users': [
					{ id: 1, username: 'john' },
					{ id: 2, username: 'jane' },
				],
				'/about': 'This is the about page',
				'/users/1': {
					id: 1,
					username: 'johndoe',
					topPostId: 53231,
					city: 'mumbai',
				},
				'/users/5': {
					id: 1,
					username: 'janedoe',
					topPostId: 32443,
					city: 'pune',
				},
				'/posts/53231': {
					id: 1,
					title: 'Really amazing post',
					slug: 'really-amazing-post',
				},
			};
			const data = pages[url];

			if (data) {
				resolve({ status: 200, data });
			} else {
				reject({ status: 404 });
			}
		}, 2000);
	});
}

// fakeRequest('/users')
// 	.then((response) => {
// 		const userId = response.data[0].id;
// 		fakeRequest(`/users/${userId}`)
// 			.then((response) => {
// 				const postId = response.data.topPostId;
// 				fakeRequest(`/posts/${postId}`)
// 					.then((response) => {
// 						console.log(response);
// 					})
// 					.catch((error) => {
// 						console.log(error);
// 					});
// 			})
// 			.catch((error) => {
// 				console.log(error);
// 			});
// 	})
// 	.catch((error) => {
// 		console.log(error);
// 	});

fakeRequest('/users')
	.then((response) => {
		const userId = response.data[0].id;
		return fakeRequest(`/users/${userId}`);
	})
	.then((response) => {
		const postId = response.data.topPostId;
		return fakeRequest(`/posts/${postId}`);
	})
	.then((response) => {
		console.log(response);
	})
	.catch((error) => {
		console.log(error);
	});
