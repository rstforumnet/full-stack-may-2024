// function getPokemon() {
// 	axios
// 		.get('https://pokeapi.co/api/v2/pokemon')
// 		.then((response) => {
// 			console.log(response);
// 		})
// 		.catch((err) => {
// 			console.log(err);
// 		});
// }

// getPokemon();

// function add(a, b) {
// 	return new Promise((resolve, reject) => {
// 		if (typeof a !== 'number' || typeof b !== 'number') {
// 			reject('a and b have to numbers');
// 		}
// 		resolve(a + b);
// 	});
// }

// add(10, 20)
// 	.then((result) => console.log(result))
// 	.catch((err) => console.log(err));

// async function add(a, b) {
// 	return a + b;
// }

// const result = add(10, 20);
// console.log(result);

// function add(a, b) {
// 	return new Promise((resolve, reject) => {
// 		if (typeof a !== 'number' || typeof b !== 'number') {
// 			reject('a and b have to numbers');
// 		}
// 		resolve(a + b);
// 	});
// }

// async function add(a, b) {
// 	if (typeof a !== 'number' || typeof b !== 'number') {
// 		throw 'a and b have to numbers';
// 	}
// 	return a + b;
// }

// add(10, '20')
// 	.then((result) => console.log(result))
// 	.catch((err) => console.log(err));

// console.log('Hello world');

// function getPokemon() {
// 	axios
// 		.get('https://pokeapi.co/api/v2/pokemon')
// 		.then((response) => {
// 			console.log(response);
// 		})
// 		.catch((err) => {
// 			console.log(err);
// 		});
// }

// async function getPokemon() {
// 	try {
// 		const result = await axios.get('https://pokeapi.co/api/v2/pokemon');
// 		console.log(result.data.results);
// 	} catch (err) {
// 		console.log(err.message);
// 	}
// }

// getPokemon();

async function getPokemon() {
	const pokemon1Promise = axios.get('https://pokeapi.co/api/v2/pokemon/1');
	const pokemon2Promise = axios.get('https://pokeapi.co/api/v2/pokemon/2');
	const pokemon3Promise = axios.get('https://pokeapi.co/api/v2/pokemon/3');
	const results = await Promise.all([
		pokemon1Promise,
		pokemon2Promise,
		pokemon3Promise,
	]);
	console.log(results);

	// const poke1 = await pokemon1Promise;
	// const poke2 = await pokemon2Promise;
	// const poke3 = await pokemon3Promise;
	// console.log(poke1.data);
	// console.log(poke2.data);
	// console.log(poke3.data);
}

getPokemon();
