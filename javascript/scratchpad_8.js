// // // DRY - WET

// // function flipCoin() {
// // 	let randomNum = Math.random();

// // 	if (randomNum > 0.5) {
// // 		console.log('HEADS');
// // 	} else {
// // 		console.log('TAILS');
// // 	}
// // }

// // flipCoin();
// // flipCoin();
// // flipCoin();
// // flipCoin();
// // flipCoin();

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice() {
// 	rollDie();
// 	rollDie();
// 	rollDie();
// }

// throwDice();

// function greet(firstName, lastName, age) {
// 	console.log(
// 		`Hello, my name is ${firstName} ${lastName} and I am ${age} years old.`
// 	);
// }

// let val = 10;

// function greet(firstName, lastName) {
// 	console.log(
// 		`Hello, my name is ${firstName} ${lastName} and I am ${val} years old.`
// 	);
// }

// greet('John', 'Doe');
// greet('Jane', 'Smith', 30);

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice(times) {
// 	for (let i = 0; i < times; i++) {
// 		rollDie();
// 	}
// }

// throwDice(20);

// function greet() {
// 	return 'Hello World';
// }

// // let result = greet();
// // console.log(result);

// console.log(greet());

// function add(a, b) {
// 	return a + b;
// }

// console.log(add(10, 20));

// function multiply(a, b) {
// 	return a * b;
// }

// function square(a) {
// 	return multiply(a, a);
// }

// console.log(square(2));

// let fullName = 'Jane Doe';

// function greet() {
// 	let fullName = 'Jack Smith';
// 	console.log(fullName);
// }

// greet();
// console.log(fullName);

// let fullName = 'John Doe';

// if (true) {
// 	var fullName = 'Jane Smith';
// 	console.log(fullName);
// }

// console.log(fullName);

// for (var i = 0; i < 10; i++) {
// 	console.log(i);
// }

// console.log('OUTSIDE THE LOOP', i);
const fullName = 'John Doe';

function outer() {
	function inner() {
		console.log(fullName);
	}

	inner();
}

outer();
