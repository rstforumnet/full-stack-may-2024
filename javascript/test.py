class User:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return self.name

    def __add__(self, another_user):
        return User("New Born", 0)


user1 = User("John Doe", 20)
user2 = User("Jane Doe", 25)

print(user1 + user2)
