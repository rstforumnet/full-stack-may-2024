// fetch('https://pokeapi.co/api/v2/pokemon')
// 	.then((response) => {
// 		return response.json();
// 	})
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log(err);
// 	});

// fetch('https://pokeapi.co/api/v2/pokemon')
// 	.then((response) => {
// 		console.log('I am in the then block');
// 		if (!response.ok) {
// 			throw new Error();
// 		}
// 		return response.json();
// 	})
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log('I am in the err block');
// 		console.log(err);
// 	});

// axios
// 	.get('https://pokeapi.co/api/v2/pokemon')
// 	.then((response) => {
// 		console.log('I am in the then block');
// 		console.log(response.data);
// 	})
// 	.catch((err) => {
// 		console.log('I am in the catch block');
// 		console.log(err);
// 	});

const root = document.querySelector('#root');
const section = document.createElement('section');
section.style.display = 'grid';
section.style.gridTemplateColumns = '1fr 1fr 1fr 1fr 1fr';
section.style.gap = '20px';
root.append(section);

axios
	.get('https://pokeapi.co/api/v2/pokemon?limit=100000')
	.then((response) => {
		for (let pokemon of response.data.results) {
			axios
				.get(pokemon.url)
				.then((res) => {
					const pokemonImg = document.createElement('img');
					pokemonImg.src =
						res.data.sprites.other['official-artwork'].front_default;
					pokemonImg.style.width = '100%';

					const pokemonCard = document.createElement('article');

					const pokemonName = document.createElement('h4');
					pokemonName.innerText = res.data.name;
					pokemonName.style.textAlign = 'center';
					pokemonName.style.fontFamily = 'Arial';
					pokemonName.style.textTransform = 'capitalize';
					pokemonName.style.marginTop = '5px';

					pokemonCard.append(pokemonImg);
					pokemonCard.append(pokemonName);
					section.append(pokemonCard);
				})
				.catch((err) => {
					console.log(err);
				});
		}
	})
	.catch((err) => {
		console.log(err);
	});
