// const form = document.querySelector('form');

// form.addEventListener('submit', function (event) {
// 	event.preventDefault();
// 	// console.log('Form was submitted');
// 	console.dir(event.target.elements.username.value);
// 	console.dir(event.target.elements.password.value);
// });

const input = document.querySelector('#username');
const p = document.querySelector('p');

input.addEventListener('input', function (event) {
	p.innerText = event.target.value.toUpperCase();
});
