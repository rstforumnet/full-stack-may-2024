// function multiply(x, y) {
// 	return x * y;
// }

// function square(x) {
// 	return multiply(x, x);
// }

// function rightTriangle(x, y, z) {
// 	return square(x) + square(y) === square(z);
// }

// rightTriangle(1, 2, 3);

// console.log('The first log');
// alert('Hello World!');
// console.log('The second log');

// console.log('The first log');
// setTimeout(function () {
// 	console.log('Hello world');
// }, 5000);
// console.log('The second log');

const btn = document.querySelector('button');

setTimeout(() => {
	btn.style.transform = 'translateX(100px)';
	setTimeout(() => {
		btn.style.transform = 'translateX(200px)';
		setTimeout(() => {
			btn.style.transform = 'translateX(300px)';
			setTimeout(() => {
				btn.style.transform = 'translateX(400px)';
				setTimeout(() => {
					btn.style.transform = 'translateX(500px)';
					setTimeout(() => {
						btn.style.transform = 'translateX(600px)';
						setTimeout(() => {
							btn.style.transform = 'translateX(700px)';
						}, 1000);
					}, 1000);
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);
}, 1000);
