// // // // const nums = [1, 2, 38975, 4, 5, 6, 7, 8, 9, 10];

// // // // nums.forEach(function (num) {
// // // // 	console.log(num, 'Hello World');
// // // // });

// // // // const langs = ['JavaScript', 'Python', 'Ruby', 'Rust', 'Elm', 'Haskell', 'C++'];

// // // // langs.forEach(function (lang) {
// // // // 	console.log(lang);
// // // // });

// // // // for (let num of nums) {
// // // // 	console.log(num);
// // // // }

// // // // const movies = [
// // // // 	{
// // // // 		title: 'Avengers',
// // // // 		rating: 1.1,
// // // // 	},
// // // // 	{
// // // // 		title: 'Dr. Strange',
// // // // 		rating: 1.9,
// // // // 	},
// // // // 	{
// // // // 		title: 'Tenet',
// // // // 		rating: 4.1,
// // // // 	},
// // // // 	{
// // // // 		title: 'Joker',
// // // // 		rating: 4.7,
// // // // 	},
// // // // ];

// // // // movies.forEach(function (movie, i) {
// // // // 	console.log(`${i}: ${movie.title} has a rating of ${movie.rating}`);
// // // // });

// // // const langs = ['JavaScript', 'Python', 'Ruby', 'Rust', 'Elm', 'Haskell', 'C++'];

// // // // const upperLangs = langs.map(function (lang) {
// // // // 	return lang.toUpperCase();
// // // // });

// // // // const upperLangs = langs.map(function (lang) {
// // // // 	return { lang: lang.toUpperCase(), length: lang.length };
// // // // });

// // // // console.log(upperLangs);

// // // console.log(
// // // 	langs.map(function (lang) {
// // // 		return { lang: lang.toUpperCase(), length: lang.length };
// // // 	})
// // // );

// // // // const upperLangs = [];

// // // // for (let lang of langs) {
// // // // 	upperLangs.push(lang.toUpperCase());
// // // // }

// // // // console.log(upperLangs);

// // // const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// // // const doubles = nums.map(function (num) {
// // // 	return num * 2;
// // // });

// // // console.log(doubles)

// // // const numDetails = nums.map(function (num) {
// // // 	return {
// // // 		number: num,
// // // 		isEven: num % 2 === 0,
// // // 	};
// // // });

// // // console.log(numDetails);

// // // const square = function (num) {
// // // 	return num ** 2;
// // // };

// // // const square = n => n ** 2;

// // // console.log(square(5));

// // // const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// // // const doubles = nums.map((num) => num * 2);

// // // console.log(doubles);

// // const square = () => {
// // 	let something = 'world';
// // 	let result = something.toLowerCase();
// // 	return 'hello world';
// // };

// let movies = ['The Terminator', 'The Avengers', 'Jurassic Park', 'Titanic'];

// let result = movies.filter((movie) => movie[0] === 'X');
// console.log(result);

// // const books = [
// // 	{
// // 		title: 'The Shining',
// // 		author: 'Stephen King',
// // 		rating: 4.1,
// // 	},
// // 	{
// // 		title: 'Sacred Games',
// // 		author: 'Vikram Chandra',
// // 		rating: 4.5,
// // 	},
// // 	{
// // 		title: '1984',
// // 		author: 'George Orwell',
// // 		rating: 4.9,
// // 	},
// // 	{
// // 		title: 'The Alchemist',
// // 		author: 'Paulo Coelho',
// // 		rating: 3.5,
// // 	},
// // 	{
// // 		title: 'The Great Gatsby',
// // 		author: 'F. Scott Fitzgerald',
// // 		rating: 3.8,
// // 	},
// // ];

// // const result = books.filter((book) => book.rating < 4);
// // console.log(result);

// const names = ['jack', 'james', 'john', 'jane', 'josh', 'jrad'];

// const result = names.some((name) => name[0] === 'b');
// console.log(result);

// const prices = [500.4, 211, 23, 5, 4, 22.2, -23.2, 9233];

// const result = prices.sort((a, b) => b - a);
// console.log(result);

// const nums = [1, 2, 3, 4, 5];

// const result = nums.reduce((acc, currVal) => acc * currVal);
// console.log(result);

// acc   currVal
// 1     2
// 3     3
// 6     4
// 10    5
// 15

// let nums = [21, 221, 2, 1, 34, 123, 4342, 56, 4];

// const max = nums.reduce((acc, currVal) => {
// 	if (currVal > acc) {
// 		return currVal;
// 	}
// 	return acc;
// });

// console.log(max);

// acc     currVal
// 21      221
// 221     2
// 221     1
// 221     34
// 221     123
// 221     4342
// 4342    56
// 4342    4
// 4342

const nums = [1, 2, 3, 4, 5];

const result = nums.reduce((acc, currVal) => acc + currVal, 100);
console.log(result);
