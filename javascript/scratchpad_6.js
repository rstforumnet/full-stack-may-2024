// // const product1 = [
// // 	'iPhone 15',
// // 	'Apple',
// // 	'Some description...',
// // 	100000,
// // 	500,
// // 	'https://....',
// // 	true,
// // ];

// // const product4 = {
// // 	name: 'iPhone 15',
// // 	brand: 'Apple',
// // 	description: 'Some description...',
// // 	inStock: 500,
// // 	imageUrl: 'https://....',
// // 	discounted: true,
// // 	price: 100000,
// // 	100: true,
// // 	'hello-world': false,
// // };

// // console.log(product2.price);
// // console.log(product2.description);
// // console.log(product2.discounted);
// // console.log(product2['hello world']);

// const pallette = {
// 	red: '#eb4d4b',
// 	yellow: '#f9ca24',
// 	blue: '#30336b',
// };

// const key = 'red';

// console.log(pallette[key]);

// for (let count = 10; count >= 0; count++) {
// 	console.log(count, 'Hello World');
// }

// for (let num = 1; num <= 10; num++) {
// 	console.log(`${num}*${num}=${num * num}`);
// }

// const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// for (let i = 0; i < nums.length; i++) {
// 	console.log(nums[i]);
// }

// const movies = [
// 	{ movieName: 'Inception', rating: 3.8 },
// 	{ movieName: 'Avengers', rating: 3.4 },
// 	{ movieName: 'Iron Man', rating: 2.9 },
// ];

// for (let i = 0; i < movies.length; i++) {
// 	const movie = movies[i];
// 	console.log(`${movie.movieName} has a rating of ${movie.rating}`);
// }

// const word = 'Hello World';

// for (let i = 0; i < word.length; i++) {
// 	console.log(word[i]);
// }

// const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// let total = 0;

// for (let i = 0; i < nums.length; i++) {
// 	total += nums[i];
// }

// console.log(total);

// const word = 'Hello World';

// let reversedStr = '';

// for (let i = word.length - 1; i >= 0; i--) {
// 	reversedStr += word[i];
// }

// console.log(reversedStr);

// for (let i = 0; i < 5; i++) {
// 	console.log(`OUTER LOOP: ${i}`);

// 	for (let j = 0; j < 5; j++) {
// 		console.log(`    INNER LOOP: ${j}`);
// 	}
// }

const gameBoard = [
	[4, 64, 8, 4],
	[128, 32, 4, 16],
	[16, 4, 4, 32],
	[2, 16, 16, 2],
];

for (let i = 0; i < gameBoard.length; i++) {
	for (let j = 0; j < gameBoard[i].length; j++) {
		console.log(gameBoard[i][j]);
	}
}
