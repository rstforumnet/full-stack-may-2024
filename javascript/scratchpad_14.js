// // const li = document.querySelector('#special');
// // // console.log(li.parentElement.parentElement.parentElement);
// // const ul = li.parentElement;

// // console.log(ul.children);

// // const lis = document.querySelectorAll('li');

// // for (let li of lis) {
// // 	setInterval(() => {
// // 		li.innerText = Math.random();
// // 	}, 1000);
// // }

// // const h1 = document.querySelector('h1');
// // h1.style.color = 'red';
// // h1.style.backgroundColor = 'yellow';

const colors = [
	'red',
	'green',
	'blue',
	'yellow',
	'teal',
	'aqua',
	'orange',
	'turquoise',
	'violet',
	'brown',
	'black',
	'white',
	'purple',
	'maroon',
];

// const nums = [12, 34, 45, 6, 40, 7, 140, 5, 200, 23];

// const lis = document.querySelectorAll('li');

// for (let li of lis) {
// 	setInterval(() => {
// 		const randomColorIdx1 = Math.floor(Math.random() * colors.length);
// 		const randomColorIdx2 = Math.floor(Math.random() * colors.length);
// 		const randomNumIdx = Math.floor(Math.random() * nums.length);
// 		li.style.color = colors[randomColorIdx1];
// 		li.style.backgroundColor = colors[randomColorIdx2];
// 		li.style.fontSize = `${nums[randomNumIdx]}px`;
// 		li.style.transition = 'all 0.2s';
// 	}, 100);
// }

// setInterval(() => {
// 	const randomColorIdx = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[randomColorIdx];
// }, 50);

// const li = document.querySelector('li');
// // li.className = 'todo my-list done';
// li.classList.add('done');
// li.classList.remove('my-item');
// li.classList.toggle('todo');

// const root = document.querySelector('#root');

// const title = document.createElement('li'); // <h1></h1>
// title.className = 'todo';
// title.innerText = 'Hello world from the DOM'; // <h1>Hello world from the DOM</h1>
// title.style.color = 'blue'; // <h1 style="color: blue;">Hello world from the DOM</h1>

// // root.appendChild(title);

// const existingLi = document.querySelectorAll('li')[1];
// console.log(existingLi);

// const ul = document.querySelector('ul');
// // ul.appendChild(title);
// ul.insertBefore(title, existingLi);

// const para = document.querySelector('p');
// const b = document.createElement('b');
// b.innerText = 'Hello World';
// const i = document.createElement('i');
// i.innerText = 'Hello World';

// para.prepend(b, i);

// para.remove();

// const button = document.querySelector('button');
// button.onclick = function () {
// 	const randomColorIdx = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[randomColorIdx];
// 	console.log('hello world');
// };
// button.onclick = function () {
// 	console.log('This is something');
// };

// const button = document.querySelector('button');

// button.addEventListener('click', function () {
// 	const randomColorIdx = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[randomColorIdx];
// 	console.log('hello world');
// });

// button.addEventListener('click', function () {
// 	console.log('This is something');
// });

// button.addEventListener('mouseup', function () {
// 	console.log('This is something else');
// });

const input = document.querySelector('input');
const button = document.querySelector('button');
const ul = document.querySelector('ul');

button.addEventListener('click', function () {
	const li = document.createElement('li');
	li.innerText = input.value;
	li.addEventListener('click', function () {
		li.classList.toggle('done');
	});
	const delButton = document.createElement('button');
	delButton.innerText = 'Delete';
	delButton.addEventListener('click', function () {
		li.remove();
	});
	li.append(delButton);

	ul.append(li);
	input.value = '';
});
