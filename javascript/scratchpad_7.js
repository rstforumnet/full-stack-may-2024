// // for (let count = 0; count < 10; count++) {
// // 	console.log(count);
// // }

// // let count = 0;

// // while (count < 10) {
// // 	console.log(count);
// // 	count++;
// // }

// // let target = Math.floor(Math.random() * 10) + 1;
// // let guess = Math.floor(Math.random() * 10) + 1;

// // while (target !== guess) {
// // 	if (guess === 5) {
// // 		break;
// // 	}
// // 	console.log(`Target: ${target} | Guess: ${guess}`);
// // 	guess = Math.floor(Math.random() * 10) + 1;
// // }

// // console.log(`Final Result:\nTarget: ${target} | Guess: ${guess}`);

// // let target = Math.floor(Math.random() * 10) + 1;
// // let guess = Math.floor(Math.random() * 10) + 1;

// // while (true) {
// // 	if (guess === target) {
// // 		break;
// // 	}
// // 	console.log(`Target: ${target} | Guess: ${guess}`);
// // 	guess = Math.floor(Math.random() * 10) + 1;
// // }

// // console.log(`Final Result:\nTarget: ${target} | Guess: ${guess}`);

// let langs = [
// 	'JavaScript',
// 	'Python',
// 	'Ruby',
// 	'Rust',
// 	'Zig',
// 	'Elixir',
// 	'Erlang',
// 	'C++',
// 	'C',
// 	'C#',
// 	'Pascal',
// 	'D',
// 	'V',
// 	'Elm',
// ];

// // for (let i = 0; i < langs.length; i++) {
// // 	console.log(langs[i]);
// // }

// // for (let lang of langs) {
// // 	console.log(lang);
// // }

// // for (let char of 'hello world') {
// // 	console.log(char);
// // }

// const matrix = [
// 	[1, 4, 7],
// 	[9, 7, 2],
// 	[9, 4, 6],
// ];

// for (let row of matrix) {
// 	for (let col of row) {
// 		console.log(col);
// 	}
// }

// const cats = ['fashion', 'mobiles', 'books'];
// const prods = ['tshirt', 'samsung', '1984'];

// for (let i = 0; i < cats.length; i++) {
// 	console.log(cats[i], prods[i]);
// }

const productPrices = {
	OnePlus: 50000,
	Samsung: 90000,
	Apple: 80000,
};

// for (let item of productPrices) {
// 	console.log(item);
// }

for (let key of Object.keys(productPrices)) {
	console.log(key, productPrices[key]);
}

for (let key in productPrices) {
	console.log(key, productPrices[key]);
}

// for (let key of Object.values(productPrices)) {
// 	console.log(key);
// }
