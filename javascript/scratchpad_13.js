// console.dir(document);
// document.body.style.backgroundColor = 'red';

// const el = document.getElementById('hello');
// console.log(el);

// const listItems = document.getElementsByTagName('li');
// console.log(listItems);

// const uList = document.getElementsByTagName('ul')[0];
// // console.log(uList);

// const listItems = uList.getElementsByTagName('li');
// console.log(listItems);

// const hellos = document.getElementsByClassName('hello');
// console.log(hellos);

// // ol > li.hello

// const testId = document.querySelector('#testId');
// console.log(testId);

// const lis = document.querySelectorAll('ul li');
// console.log(lis);

// const p = document.querySelector('#testId');

// // console.dir(p.innerText);

// setInterval(function () {
// 	p.innerText = Math.random();
// }, 100);

// const ul = document.querySelector('ul');
// // console.log(ul.innerText);

// ul.innerText = 'Hello World';

// const para = document.querySelector('p');

// // console.log(para.innerText);

// para.innerHTML = 'Hello world from <strong>JavaScript</strong>!';
// para.innerHTML += Math.random();

const a = document.querySelector('a');
// console.log(a.href);
// console.log(a.getAttribute('href'));

// console.log(a.id);
// console.log(a.getAttribute('id'));

// a.id = 'helloWorld';
a.setAttribute('id', 'goodByeWorld');

// a.href = 'https://bing.com';
// a.id = 'helloWorld';
// a.className = 'hello';
// console.log(a);
