// // // function raiseBy(a = 1, b = 1) {
// // // 	return a ** b;
// // // }

// // // console.log(raiseBy(2, 5));

// // nums = [20, 30, 1, 23, 453456, 4];

// // Math.max(20, 30, 1, 23, 453456, 4)

// function dummy(a, b, c) {
// 	console.log(a);
// 	console.log(b);
// 	console.log(c);
// }

// const names = ['John', 'Jane', 'Jack'];

// // dummy(names[0], names[1], names[2]);
// // dummy(...names);
// dummy(...'hello');

// function add(a, b, ...nums) {
// 	console.log(a);
// 	console.log(b);
// 	console.log(nums);
// }

// function add(...nums) {
// 	let total = 0;
// 	for (let num of nums) {
// 		total += num;
// 	}
// 	return total;
// }

// console.log(add(10, 20, 30, 40, 50, 60, 70));

// const users = ['john', 'jane', 'jack'];

// // const admin = users[0];
// // const mod = users[1];
// // const sub = users[2];

// // const [admin, , sub] = users;
// const [admin, ...others] = users;

// console.log(admin, others);

// const user = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	email: 'john.doe@gmail.com',
// 	phone: 99982234567,
// };

// const { firstName, email: emailAddress, ...others } = user;

// console.log(firstName, emailAddress, others);

function profile({ firstName, lastName, age }) {
	console.log(
		`Hello, my name is ${firstName} ${lastName} and I am ${age} years old.`
	);
}

profile({ firstName: 'Jane', lastName: 'Doe', age: 20 });
