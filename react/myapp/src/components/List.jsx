const List = () => {
	const movies = [
		{ id: 'mov1', value: 'The Last Samurai' },
		{ id: 'mov2', value: 'Van Helsing' },
		{ id: 'mov3', value: 'Braveheart' },
		{ id: 'mov4', value: 'A Few Good Men' },
		{ id: 'mov4', value: 'Star Wars: Episode III - Revenge of the Sith' },
	];

	return (
		<div>
			<h1>My List</h1>
			<ul>
				{movies.map((movie, i) => (
					<li key={i}>{movie.value}</li>
				))}
			</ul>
		</div>
	);
};

export default List;
