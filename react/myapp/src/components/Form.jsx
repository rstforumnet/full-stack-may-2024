import { useRef, useState } from 'react';

const Form = () => {
	const [error, setError] = useState(null);
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const usernameRef = useRef();
	const passwordRef = useRef();

	const handleSubmit = (e) => {
		e.preventDefault();
		// console.log(e.target[0].value);
		// console.log(e.target[1].value);
		// const { username, password } = e.target.elements;
		// console.log(username.value);
		// console.log(password.value);
		// console.log(e.target.elements.username.value);
		// console.log(e.target.elements.password.value);
		// console.log(usernameRef.current.value);
		// console.log(passwordRef.current.value);
		// usernameRef.current.type = 'number';
	};

	// const handleUsernameChange = (e) => {
	// 	if (e.target.value !== e.target.value.toLowerCase()) {
	// 		setError('Username cannot contain uppercase chars');
	// 	} else {
	// 		setError(null);
	// 	}
	// };

	return (
		<div>
			<h3>My Form</h3>

			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor='username'>Username</label>
					<br />
					<input
						ref={usernameRef}
						value={username}
						type='text'
						id='username'
						onChange={(e) => setUsername(e.target.value.toLowerCase())}
					/>
				</div>
				<span style={{ color: 'red', fontWeight: 'bold' }}>{error}</span>
				<div>
					<label htmlFor='password'>Password</label>
					<br />
					<input
						ref={passwordRef}
						value={password}
						type='text'
						id='password'
						onChange={(e) => setPassword(e.target.value)}
					/>
				</div>
				<br />
				<button>Submit</button>
			</form>
		</div>
	);
};

export default Form;
